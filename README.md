# gps车辆定位管理系统

#### 介绍
通过给车辆安装gps定位器，实现车辆的监控管理

#### 软件架构
软件架构说明
主要框架
python：
django 
twisted
数据库：postgresql
前端：layui
进程守护：supervisor
web服务器：nginx
中间件：redis


#### 安装和使用教程
web服务器：
1、postgresql数据库新建 car 数据库,也可自行修改setting里面的数据库配置
2、运行python manage.py runserver 有缺少的库，自行安装下

数据处理模块：
data_handle文件夹
1、receive_serv.py,数据接收，并解析，解析出来时候放入redis，设备的监听是10808，默认协议是jt808部标通讯协议，目前也只解析了定位和一些基本参数。
2、store_serv.py，从redis接受解析好的数据，进行状态计算（超速报警，围栏报警等）和存储




可以自己登录http://114.67.122.37:10889

入行时候的作品，比较粗糙，甚至愚蠢，不喜勿喷。
微调之后，可以直接用于小型车队的调度管理。

最后放些效果图
![输入图片说明](car1.jpg)
![输入图片说明](car2.jpg)
![输入图片说明](car_apply.png)
![输入图片说明](car3.png)
![输入图片说明](move_line.png)




