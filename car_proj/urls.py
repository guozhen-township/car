"""car_proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path,include
app_name='car_app'

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'',include('car_app.urls',namespace='car_app')),
    # url(r'users/',include('users.urls',namespace='users')),
    # url(r'login/', views.LoginView.as_view()),
    # url(r'logins/', views.LoginView.as_view()),
    # url(r'get_trail_api/', views.get_trail_View.as_view()),
    # url(r'all_on_line/', views.AllOnLine.as_view()), 
    # url(r'time_now/', views.TimeNowView.as_view()),
    # url(r'websocket_test/',views.websocket_test),
    
]
