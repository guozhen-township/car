#动态生成设备信息存储表格，每个表格生成一张独立的表格用于存储实时上传的数据
from sqlalchemy import Column,String,Integer,create_engine,Text
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import desc
from sqlalchemy.ext.automap import automap_base	#导入数据库映射，表示用sqlachemy操作django的model创建的数据库
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy import MetaData
import datetime



Base=declarative_base()
engine = create_engine('postgresql+psycopg2://postgres:123456@localhost:5432/car')	#连接django在使用的postgresql数据库
session = Session(engine)

dBase = automap_base()
dBase.prepare(engine, reflect=True)	

def creat_device_table(dev_id):
	Base=declarative_base()
	engine = create_engine('postgresql+psycopg2://postgres:123456@localhost:5432/car')	#连接django在使用的postgresql数据库
	session = Session(engine)

	class Device(Base):
		__tablename__ = dev_id
		id = Column(Integer, primary_key=True)
		device_id=Column(String,default=0)#'设备ID'
		lng=Column(String,default=0)#'经度'
		lat=Column(String,default=0)#'纬度'
		lbs=Column(String,default=0)#'基站信息')
		wifi=Column(String,default=0)#'WIFI热点信息')
		rssi=Column(String,default=0)#'信号强度'
		heart_rate=Column(String,default=0)#'心律值'
		blood_pres=Column(String,default=0)#'血压值'
		step_count=Column(String,default=0)#'步数'
		obd=Column(Text,default=0)#'obd信息'
		track_type=Column(String,default=0)#'定位类型'
		speed=Column(String,default=0)#'设备速度'
		dirct=Column(String,default=0)#'设备方向'
		status=Column(String,default=0)#'设备状态'
		battery=Column(String,default=0)#'设备电量'
		sos_alarm=Column(String,default=0)#sos报警')
		dev_upload=Column(String,default=0)#'设备上传时间'
		serv_receive=Column(String,default=0)#'服务器接收时间'
		hard_verson=Column(Text,default=0)#'设备版本信息'
		mileage=Column(String,default=0)#'设备里程'
		oil_use=Column(String,default=0)#'设备油耗'
		efence_alarm=Column(String,default=0)#'围栏报警')
		speed_alarm=Column(String,default=0)#'超速报警')
		stop_time=Column(String,default=0)#'停留次数')
		heart_alarm=Column(String,default=0)#'心率报警')
		blood_alarm=Column(String,default=0)#'血压报警')
		location=Column(String,default=0)#'位置信息')
		othermesg=Column(String,default=0)#'其他信息')
		scanner=Column(Text,default=0)#'读卡器'
		remark=Column(String,default=0)#'备注'
	

	Base.metadata.create_all(engine)
	session.commit()

# 删除表格
def drop_table(table_name):

	
	table = dBase.classes[table_name]
	# print('table==>',table)
	if table is not None:
		table.__table__.drop(engine)
		# print('drop table success')
		return 'drop table success'
	else:
		# print('failed')
		return 'failed'

# creat_device_table('67890')